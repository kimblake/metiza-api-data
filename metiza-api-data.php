<?php
/*
Plugin Name: AppPresser Metiza API code
Plugin URI: http://apppresser.com
Description: How to manipulate the WP-API for your app.
Version: 0.1
Author: AppPresser Team
Author URI: http://apppresser.com
License: GPLv2
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
if (! function_exists('appp_register_template_hook' || 'appp_register_home_page')) {
    add_action('rest_api_init', 'appp_register_template_hook');
    add_action('rest_api_init', 'appp_register_home_page');
}

/**
 * Registering WP REST API Endpoint
 *
 * @pram appp/homepage
 */
function appp_register_home_page()
{
    register_rest_route(
        'homepage',
        'appp',
        [
            'callback'        => 'appp_home_page',
            'methods'         => 'GET',
            'update_callback' => null,
            'schema'          => null,
        ]
    );
}

function appp_register_template_hook()
{
    register_rest_field(
        'post',
        'appp',
        [
            'get_callback'    => 'appp_get_hook_data',
            'update_callback' => null,
            'schema'          => null,
        ]
    );
}

/**
 * WP REST API callback hooks
 *
 * @pram appp_home_page  homepage cats data
 * @pram appp_get_hook_data
 */

/**
 * @return array|string
 */
function appp_home_page()
{

    $data       = [];
    $homepage   = [];
    $categories = get_categories([ 'parent' => 0 ]);
    $sorted     = sort_parent_category_recent_post($categories);
    $i=0;
    $len = count($sorted);
    foreach ($sorted as $category) {
        if ($category['term_id'] !== null) {
            $args = [
                'cat'              => $category['term_id'],
                'post_type'        => 'post',
                'posts_per_page'   => 1,
                //'post__not_in'     => $do_not_duplicate, //#this will never work unless you define global $post
                'category__not_in' => 'featured',
            ];

            $query = new WP_Query($args);
            $query->the_post();
            $post_id = get_the_ID();
            $cat = get_category_ext($post_id, $type = 'nolink');
            // preparing styled output of content
            $formatted = '<div class="related-thumb">
								<a rel="external" href="' . $cat['url'] . '">
									<div class="row">
										<div class="col-md-6">' . get_the_post_thumbnail($cat['term_id'], 'large') . '</div>
									</div>
								</a>
							</div>';
            // response array
            $homepage[] = [
                'id'       => $post_id,
                'term_id'  => $category['term_id'],
                'slug'     => $category['slug'],
                'date'     => $category['post_date'],
                'date_gmt' => $category['post_date'],
                'title'    => [ 'rendered' => $cat['name'] ],
                'appp'     => [
//					'post_list'   => [ 'above_title' => $cat_name ],
                    'post_list'   => [ 'above_title' => $formatted ],
                ],
            ];

            wp_reset_query();
        }
        $i++;
        if ($i ===$len) {
            break;
        }
    }

    return $homepage;
}

/**
 * Get the value of a meta field field
 *
 * @param array           $object     Details of current post.
 * @param string          $field_name Name of field.
 * @param WP_REST_Request $request    Current request
 *
 * @return mixed
 */
function appp_get_hook_data($object, $field_name, $request)
{

    $post_id = $object['id']; // standardized post_id
    $post    = get_post($post_id, ARRAY_A);
    extract($post); // $post['post_date'] =  $post_date
    $author_bio_avatar_size = apply_filters('metiza_author_bio_avatar_size', 60);
    $date       = mysql2date("M. j, Y", $post_date);
    $categories = get_the_category($post_id);
    $cat        = get_child_category_ext($post_id, 'nolink');

//	print_r( $catTitle );die;
    $data       = [];

    $data['post_list']['above_title'] = '<span class="child-cat">' . $cat['name'] . '</span>';

    $data['post_detail']['above_title'] .= '<div class="post-featured-wrap">' . get_the_post_thumbnail(
        $post_id,
        'large',
        [ 'class' => 'post-featured' ]
    ) . '</div>';

    $data['post_detail']['above_title'] .= '<span class="cat-links">' . get_child_category($post_id) . '</span>';


    $data['post_detail']['below_title'] = '<div class="author-meta">' . get_avatar($post_author, $author_bio_avatar_size, $post_id) .

                                          '<span class="byline"> By: <a href="' . get_author_posts_url($post_author) . '"rel="author" class="author-link">'

                                          . get_the_author_meta(
                                              'first_name',
                                              $post_author
                                          ) . ' ' . get_the_author_meta('last_name', $post_author) . '</a><br/> 

	                                       <span class="date">Posted: ' . $date . '</span></span> </div>' .

                                          short_description($post_id);

    $data['post_detail']['below_content'] = get_related_articles($categories);


    return $data;
}

// Add your custom functions here

/**
 * Get Child Category Parent with formatting output of link or array values.
 *
 * @param null $post_id
 * @param bool $type
 *
 * @return array|bool|string
 */
function get_child_category_ext($post_id = null, $type = false)
{

    $post_id = ( $post_id !== false ) ? $post_id : get_the_ID();

    if ($type == 'nolink') {
        foreach (( get_the_category($post_id) ) as $category) {
            if ($category->category_parent != 0) {
                return $category = [
                    'name'   => $category->name,
                    'cat_id' => $category->term_id,
                    'slug'   => $category->slug,
                    'url'   => get_category_link($category->term_id),
                ];
            }
        }
    } else {
        foreach (( get_the_category($post_id) ) as $category) {
            if ($category->category_parent != 0) {
                return '<a href="' . get_category_link($category->term_id) . '" title="' . esc_attr(strip_tags($category->name)) . '" ' . '>' . $category->name . '</a> ';
            }
        }
    }

    return false;
}
/**
 * Get Child Category Parent with formatting output of link or array values.
 *
 * @param null $post_id
 * @param bool $type
 *
 * @return array|bool|string
 */

function get_category_ext($post_id, $type = false)
{

    $post_id = ( $post_id !== false ) ? $post_id : get_the_ID();

    if ($type == 'nolink') {
        foreach (( get_the_category($post_id) ) as $category) {
            if ($category->category_parent <= 0) {
                return $category = [
                    'name'   => $category->name,
                    'cat_id' => $category->term_id,
                    'slug'   => $category->slug,
                    'url'   => get_category_link($category->term_id),
                ];
            }
        }
    } else {
        foreach (( get_the_category($post_id) ) as $category) {
            if ($category->category_parent <= 0) {
                return '<a href="' . get_category_link($category->term_id) . '" title="' . esc_attr(strip_tags($category->name)) . '" ' . '>' . $category->name . '</a> ';
            }
        }
    }

    return false;
}

/**
 * Sort Parent Category By Recent Posts
 *
 * @author Daniel Willitzer
 *
 * @param  null| (array) $categories defaults to current page id or uses defined categories (array)
 *
 * @return array
 */
function sort_parent_category_recent_post($categories = null)
{

    $categories = ( $categories !== false ) ? $categories : get_categories([ 'parent' => 0 ]);

    $recentposts = [];

    //sort category by most recent post
    foreach ($categories as $category) {
        $args2        = [
            'cat'              => $category->term_id,
            'post_type'        => 'post',
            'posts_per_page'   => 1,
            'category__not_in' => 'featured',
            'numberposts'      => '1',
        ];
        $recent_posts = wp_get_recent_posts($args2);

        foreach ($recent_posts as $recent) {
            $cat           = get_category_ext($recent['ID'], 'nolink');
            $recentposts[] = [
                'ID'              => $recent['ID'],
                'post_title'      => $recent['post_title'],
                'post_date'       => $recent['post_date'],
                'post_date_gmt'   => $recent['post_date_gmt'],
                'parent_category' => $cat['name'],
                'term_id'         => $cat['cat_id'],
                //'recent_post' => $recent, // debug checking post content
            ];
        }
    }
    array_sort_by_column($recentposts, 'post_date', SORT_DESC);

    return $categories = $recentposts;
}

function get_related_articles($post_id = null, $categories = null)
{


    $post_id    = ( $post_id !== false ) ? $post_id : get_the_ID();
    $categories = ( $categories !== false ) ? $categories : get_categories(get_the_ID());

    $related_posts = [];
    $category_ids  = [];
    //if(!$post_id) return 'No post id'; // dealbreaker
    //if(!$categories) return 'No Categories'; // dealbreaker
    if ($categories) {
        // sort category by most recent post
        foreach ($categories as $individual_category) {
            $category_ids[] = $individual_category->term_id;
        }

        $args = [
            'category__in'   => $category_ids,
            'post__not_in'   => [ $post_id ],
            'posts_per_page' => 4, // Number of related posts that will be shown.
            'get_posts'      => 1,
        ];

        $query = new WP_Query($args);
        $i     = 0;
        foreach ($query as $item) {
            $post_id = get_the_ID();
            // preparing styled output of content
            $formatted = '<div class="related-thumb">
						<a rel="external" href="' . get_the_permalink($post_id, 'related') . '">
							<div class="row">
								<div class="col-md-6">' . get_the_post_thumbnail($post_id, 'related') . '</div>
								<div class="col-md-6"><h3>' . get_the_title($post_id) . '</h3></div>
							</div>
						</a>
				  </div>';
            $related_posts[] = [ 'content' => $formatted ];
//			$related_posts[] = [
//				'id'       => $post_id,
////				'term_id'  => $category['term_id'],
////				'slug'     => $category['slug'],
////				'date'     => $category['post_date'],
////				'date_gmt' => $category['post_date'],
////				'title'    => [ 'rendered' => $category['post_title'] ],
//				'content'  => [$formatted],
//			];
            wp_reset_query();
            $i ++;
            if ($i == 4) {
                break;
            }
            array_push($related_posts, $formatted);
        }
        echo $related_posts;
        //array_sort_by_column( $recentposts, 'post_date', SORT_DESC);
    }

    return $categories = $related_posts;
}

/**
 * Sort Multidimensional Arrays
 *
 * @param     $array     array you want to sort
 * @param     $column    array key to sort against
 * @param int $direction SORT_DESC, SORT_ASC ..
 */
function array_sort_by_column(&$array, $column, $direction = SORT_ASC)
{
    $reference_array = [];

    foreach ($array as $key => $row) {
        if ($array[ $key ] !== null) {
            $reference_array[ $key ] = $row[ $column ];
        }
    }

    $array = array_filter(array_map('array_filter', $array));
    array_multisort($reference_array, $direction, $array);
}

if (!function_exists('get_child_category')) {
    function get_child_category()
    {
        foreach (( get_the_category() ) as $category) {
            if ($category->category_parent != 0) {
                return '<a href="' . get_category_link($category->term_id) . '" title="' . esc_attr(strip_tags($category->name)) . '" ' . '>' . $category->name . '</a> ';
            }
        }

        return false;
    }
}
if (!function_exists('get_child_category_nolink')) {
    function get_child_category_nolink()
    {
        foreach (( get_the_category() ) as $category) {
            if ($category->category_parent != 0) {
                return $category->name;
            }
        }

        return false;
    }
}
if (!function_exists('short_description')) {
    function short_description($post_id = null)
    {
        $post_id = ( $post_id !== false ) ? $post_id : get_the_ID();

        $short_description = get_field('short_description', $post_id);

        if (! empty($short_description)) {
            return '<div class="short-description"> ' . $short_description . ' </div>';
        }

        return false;
    }
}
